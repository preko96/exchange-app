import React from 'react'
import { AppRegistry, YellowBox } from 'react-native'
import { Provider } from 'react-redux'
import { name as appName } from './app.json'
import App from './src/App'
import store from './src/redux/store'
import './ReactotronConfig'
import Reactotron from 'reactotron-react-native'

YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader'])

const EnhancedApp = () =>
	<Provider store={store}>
		<App/>
	</Provider>

const OverlayedApp = Reactotron.overlay(EnhancedApp)

AppRegistry.registerComponent(appName, () => OverlayedApp)
