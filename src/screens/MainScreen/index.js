import React from 'react'
import { Container, Header, Content, Body, Title } from 'native-base'
import Flex from '../../components/Flex/'
import Exchanger from '../../containers/Exchanger/'

const MainScreen = () =>
	<Container>
		<Header>
			<Body>
				<Title>Exchange</Title>
			</Body>
		</Header>
		<Content contentContainerStyle={{ flex: 1 }}>
			<Flex>
				<Exchanger/>
				<Exchanger isTo/>
			</Flex>
		</Content>
	</Container>

export default MainScreen