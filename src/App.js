import React from 'react'
import { connect } from 'react-redux'
import { Root, StyleProvider } from 'native-base'
import { Platform, UIManager } from 'react-native'
import getTheme from '../native-base-theme/components/'
import { onStartApp } from './redux/util/actions'
import Nav from './Nav'

class App extends React.Component {
	
	componentDidMount() {
		if (Platform.OS === 'android') {
			//on android we can't use LayoutAnimations by default.
			UIManager.setLayoutAnimationEnabledExperimental(true)
		}
		this.props.onStartApp()
	}

	render() {
		return(
			<StyleProvider style={getTheme()}>
				<Root>
					<Nav/>
				</Root>
			</StyleProvider>
		)
	}
}

const mapDispatchToProps = dispatch => ({
	onStartApp: () => dispatch(onStartApp())
})

export default connect(null, mapDispatchToProps)(App)
