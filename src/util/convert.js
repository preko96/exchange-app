import isValidText from './isValidText'

export default (value, from, to, exchangeRates) => {
	if (isValidText(value) && isValidText(from) && isValidText(to) && value !== '0') {
		const result = exchangeRates[to] / exchangeRates[from] * value
		return result.toFixed(2)
	} else {
		return 0
	}
}