import { StyleSheet } from 'react-native'

export default StyleSheet.create({
	container: { padding: 20, alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row' },
	convertedText: { color: 'black', fontSize: 30, fontWeight: 'bold' },
	convertInput: { color: 'white', fontSize: 30, fontWeight: 'bold', flex: 0, paddingLeft: 0 },
})