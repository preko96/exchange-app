import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { TouchableOpacity } from 'react-native'
import { View, Text, Icon, Input } from 'native-base'

import convert from '../../util/convert'
import { onChangeFrom, onChangeTo, onChangeValue, onClearValue } from '../../redux/currency/actions'
import Flex from '../../components/Flex/'
import CurrencyPicker from '../CurrencyPicker/'
import styles from './styles'

class Exchanger extends React.Component {

	//selectors
	getSelected = () => this.props.isTo ? this.props.to : this.props.from
	getCode = () => this.getSelected().slice(0, 2)
	getName = () => this.props.names[this.getSelected()] || ''
	getOnChange = () => this.props.isTo ? this.props.onChangeTo : this.props.onChangeFrom
	onChangeText = text => this.props.onChangeValue(text)

	render() {
		//isTo => is convert to?
		const { isTo, value, from, to, exchangeRates, style, ...rest } = this.props
		return(
			<Flex style={[{backgroundColor: `${isTo ? 'white': 'rgb(230, 60, 60)'}`}, styles.container, style]} {...rest}>
				<View>
					{
						isTo ?
							<Text style={styles.convertedText}>
								{ convert(value, from, to, exchangeRates) }
							</Text> :
							<Input 
								value={value} 
								onChangeText={this.onChangeText} 
								style={styles.convertInput}
							/>
					}
					<CurrencyPicker 
						isTo={isTo}
						flag={this.getCode()} 
						onValueChange={this.getOnChange()} 
						selectedValue={this.getSelected()} 
					/>
					<Text style={{ color: `${isTo ? 'black': 'white'}`, fontSize: 12 }}>
						{ this.getName() }
					</Text>
				</View>
				{
					isTo ?
						null :
						<View>
							<TouchableOpacity onPress={this.props.onClearValue}>
								<Icon 
									name='close' 
									style={{ color: `${isTo ? 'black': 'white'}`, fontSize: 50 }} 
								/>
							</TouchableOpacity>
						</View>
				}
			</Flex>
		)
	}
}

Exchanger.propTypes = {
	style: PropTypes.object,
	exchangeRates: PropTypes.any, 
	names: PropTypes.any, 
	from: PropTypes.string, 
	to: PropTypes.string, 
	value: PropTypes.string,
	isTo: PropTypes.bool,
	onChangeValue: PropTypes.func,
	onChangeTo: PropTypes.func,
	onChangeFrom: PropTypes.func,
	onClearValue: PropTypes.func
}

const mapStateToProps = state => ({
	exchangeRates: state.exchangeRates.data,
	names: state.names.data,
	from: state.currency.from,
	to: state.currency.to,
	value: state.currency.value
})

const mapDispatchToProps = dispatch => ({
	onChangeFrom: currency => dispatch(onChangeFrom(currency)),
	onChangeTo: currency => dispatch(onChangeTo(currency)),
	onChangeValue: value => dispatch(onChangeValue(value)),
	onClearValue: () => dispatch(onClearValue())
})

export default connect(mapStateToProps, mapDispatchToProps)(Exchanger)