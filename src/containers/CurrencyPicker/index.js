import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { View, Picker, Icon } from 'native-base'
import Flag from 'react-native-round-flags'
import styles from './styles'

const CurrencyPicker = ({ isTo, exchangeRates, names, flag, onValueChange, selectedValue }) => 
	<View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 3 }}>
		<Flag code={flag} style={{ height: 20, width: 20 }}/>
		<Picker
			onValueChange={onValueChange}
			selectedValue={selectedValue}
			headerBackButtonTextStyle={{ color: 'white' }}
			note
			placeholder="Select a currency"
			textStyle={[ {color: `${isTo ? 'black': 'white'}`}, styles.pickerText ]}
			mode="dropdown"
			style={styles.picker}
			iosIcon={
				<Icon 
					name="ios-arrow-down-outline" 
					style={{ color: `${isTo ? 'black': 'white'}`, height: 28 }} 
				/>
			}
		>	
			{
				Object.keys(exchangeRates).map(currency=>
					<Picker.Item 
						key={currency} 
						label={`${names[currency]} (${currency})`} 
						value={currency}
					/>
				)
			}
		</Picker>
	</View>

CurrencyPicker.propTypes = {
	isTo: PropTypes.bool,
	flag: PropTypes.string,
	exchangeRates: PropTypes.any,
	names: PropTypes.any,
	selectedValue: PropTypes.string,
	onValueChange: PropTypes.func,
}

const mapStateToProps = state => ({
	exchangeRates: state.exchangeRates.data,
	names: state.names.data,
})

export default connect(mapStateToProps)(CurrencyPicker)