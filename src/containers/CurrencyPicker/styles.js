import { StyleSheet } from 'react-native'

export default StyleSheet.create({
	picker: { paddingTop: 0, paddingBottom: 0 },
	pickerText: { paddingLeft: 8, paddingRight: 0 },
})

