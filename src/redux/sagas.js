import { fork } from 'redux-saga/effects'
import util from './util/saga'
import exchangeRates from './exchangeRates/saga'
import names from './names/saga'

const sagas = [
	util,
	exchangeRates,
	names
]

export default function* rootSaga() {
	yield sagas.map(saga => fork(saga))
}
