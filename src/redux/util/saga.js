import { all, takeEvery, put } from 'redux-saga/effects'
import { onStartFetchRates } from '../exchangeRates/actions'
import { onFetchnamesStart } from '../names/actions'
import { ON_START_APP } from './constants'

function* startAppWorker() {
	yield put(onStartFetchRates())
	yield put(onFetchnamesStart())
}

export default function* rootSaga() {
	yield all([
		takeEvery(ON_START_APP, startAppWorker)
	])
}
