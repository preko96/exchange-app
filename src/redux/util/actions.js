import {
	ON_START_APP,
} from './constants'

export const onStartApp = () => ({
	type: ON_START_APP,
})