import createReduxModule from '../../util/createReduxModule'

const module = name => createReduxModule('Util', name)

export const ON_START_APP = module('ON_START_APP')
export const ON_SAVE_CURRENCIES_HELPER = module('ON_SAVE_CURRENCIES_HELPER')