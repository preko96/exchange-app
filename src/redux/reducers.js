import exchangeRates from './exchangeRates/reducer'
import util from './util/reducer'
import currency from './currency/reducer'
import names from './names/reducer'

export default {
	util,
	exchangeRates,
	currency,
	names
}

