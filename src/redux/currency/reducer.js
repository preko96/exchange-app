import {
	ON_CHANGE_FROM,
	ON_CHANGE_TO,
	ON_CHANGE_VALUE,
	ON_CLEAR_VALUE
} from './constants'

const initState = {
	value: '0',
	from: '',
	to: '',
}

export default (state=initState, action={}) => {
	switch(action.type) {
	case ON_CLEAR_VALUE: 
		return { ...state, value: '0' }
	case ON_CHANGE_VALUE:
		return { ...state, value: action.payload }
	case ON_CHANGE_FROM: {
		return { ...state, from: action.payload }
	}
	case ON_CHANGE_TO: 
		return { ...state, to: action.payload }
	default:
		return state
	}
}