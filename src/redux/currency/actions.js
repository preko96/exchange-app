import { 
	ON_CHANGE_FROM,
	ON_CHANGE_TO,
	ON_CHANGE_VALUE,
	ON_CLEAR_VALUE
} from './constants'

export const onChangeFrom = currency => ({
	type: ON_CHANGE_FROM,
	payload: currency
})

export const onChangeTo = currency => ({
	type: ON_CHANGE_TO,
	payload: currency
})

export const onChangeValue = value => ({
	type: ON_CHANGE_VALUE,
	payload: value
})

export const onClearValue = () => ({
	type: ON_CLEAR_VALUE
})