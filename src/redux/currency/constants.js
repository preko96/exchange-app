import createReduxModule from '../../util/createReduxModule'

const module = name => createReduxModule('Currency', name)

export const ON_CHANGE_FROM = module('ON_CHANGE_FROM')
export const ON_CHANGE_TO = module('ON_CHANGE_TO')
export const ON_CHANGE_VALUE = module('ON_CHANGE_VALUE')
export const ON_CLEAR_VALUE = module('ON_CLEAR_VALUE')