import {
	ON_FETCH_NAMES_START,
	ON_FETCH_NAMES_SUCCESS,
	ON_FETCH_NAMES_FAIL
} from './constants'

export const onFetchnamesStart = () => ({
	type: ON_FETCH_NAMES_START,
})

export const onFetchNamesSuccess = names => ({
	type: ON_FETCH_NAMES_SUCCESS,
	payload: names
})

export const onFetchNamesFail = error => ({
	type: ON_FETCH_NAMES_FAIL,
	payload: error
})