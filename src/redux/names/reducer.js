import {
	ON_FETCH_NAMES_START,
	ON_FETCH_NAMES_SUCCESS,
	ON_FETCH_NAMES_FAIL,
} from './constants'

const initState = {
	data: [],
	fail: null,
	pending: false,
}

export default (state=initState, action={}) => {
	switch(action.type) {
	case ON_FETCH_NAMES_START:
		return { ...state, pending: true }
	case ON_FETCH_NAMES_SUCCESS:
		return { ...state, data: action.payload, pending: false }
	case ON_FETCH_NAMES_FAIL: 
		return { ...state, fail: action.payload, pending: false }
	default:
		return state
	}
}