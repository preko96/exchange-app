import { all, takeEvery, call, put } from 'redux-saga/effects'
import axios from 'axios'
import { onFetchNamesSuccess, onFetchNamesFail } from './actions'
import { ON_FETCH_NAMES_START } from './constants'

function* fetchNamesWorker() {
	try {
		const response = yield call(axios.get, 'https://openexchangerates.org/api/currencies.json')
		const data = response.data
		yield put(onFetchNamesSuccess(data))
	} catch(error) {
		yield put(onFetchNamesFail(error))
	}
}

export default function* rootSaga() {
	yield all([
		takeEvery(ON_FETCH_NAMES_START, fetchNamesWorker)
	])
}