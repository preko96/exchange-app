import createReduxModule from '../../util/createReduxModule'

const module = name => createReduxModule('Names', name)

export const ON_FETCH_NAMES_START = module('ON_FETCH_NAMES_START')
export const ON_FETCH_NAMES_SUCCESS = module('ON_FETCH_NAMES_SUCCESS')
export const ON_FETCH_NAMES_FAIL = module('ON_FETCH_NAMES_FAIL')