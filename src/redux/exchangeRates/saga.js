import { all, takeEvery, call, put } from 'redux-saga/effects'
import Config from 'react-native-config'
import axios from 'axios'
import { onFetchRatesFail, onFetchRatesSuccess } from './actions'
import { ON_START_FETCH_RATES } from './constants'

function* fetchDataWorker() {
	const API_URL = 'http://data.fixer.io/api/latest?access_key='
	const API_ROUTE = Config.API_ROUTE
	const OPTIONS = '&symbols=USD,AUD,CAD,PLN,MXN,GBP,HUF&format=1'
	try {
		const response = yield call(axios.get, `${API_URL}${API_ROUTE}${OPTIONS}`)
		const data = response.data
		yield put(onFetchRatesSuccess(data.rates))
	} catch(error) {
		yield put(onFetchRatesFail(error))
	}
}

export default function* rootSaga() {
	yield all([
		takeEvery(ON_START_FETCH_RATES, fetchDataWorker)
	])
}