import {
	ON_START_FETCH_RATES,
	ON_FETCH_RATES_FAIL,
	ON_FETCH_RATES_SUCCESS
} from './constants'

export const onStartFetchRates = () => ({
	type: ON_START_FETCH_RATES
})

export const onFetchRatesFail = fail => ({
	type: ON_FETCH_RATES_FAIL,
	payload: fail
})

export const onFetchRatesSuccess = data => ({
	type: ON_FETCH_RATES_SUCCESS,
	payload: data
})