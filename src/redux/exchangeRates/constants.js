import createReduxModule from '../../util/createReduxModule'

const module = name => createReduxModule('ExchangeRates', name)

export const ON_START_FETCH_RATES = module('ON_START_FETCH_RATES')
export const ON_FETCH_RATES_FAIL = module('ON_FETCH_RATES_FAIL')
export const ON_FETCH_RATES_SUCCESS = module('ON_FETCH_RATES_SUCCESS')
