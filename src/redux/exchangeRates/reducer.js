import {
	ON_START_FETCH_RATES,
	ON_FETCH_RATES_FAIL,
	ON_FETCH_RATES_SUCCESS,
} from './constants'

const initState = {
	data: [],
	currencies: [],
	fail: null,
	pending: false,
}

export default (state=initState, action={}) => {
	switch(action.type) {
	case ON_START_FETCH_RATES:
		return { ...state, pending: true }
	case ON_FETCH_RATES_SUCCESS:
		return { ...state, data: action.payload, pending: false }
	case ON_FETCH_RATES_FAIL: 
		return { ...state, fail: action.payload, pending: false }
	default:
		return state
	}
}