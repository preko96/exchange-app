import React from 'react'
import PropTypes from 'prop-types'
import { View } from 'native-base'
 
const Flex = ({ children, ...rest }) => 
	<View flex={1} {...rest}>
		{ children }
	</View>

Flex.propTypes = {
	children: PropTypes.node
}

export default Flex