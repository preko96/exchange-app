import React from 'react'
import { createStackNavigator, createSwitchNavigator } from 'react-navigation'
import MainScreen from '../src/screens/MainScreen/'

const createScreenObject = screen => ({
	screen: screen,
	navigationOptions: { 
		header: null
	}
})

const AppStack = createStackNavigator(
	{	
		MainScreen: createScreenObject(MainScreen),
	},
	{
		initialRouteName: 'MainScreen'
	}
)

const SwitchNav = createSwitchNavigator(
	{
		App: AppStack,
	},
	{
		initialRouteName: 'App'
	}
)

const Navigation = () => <SwitchNav/> 

export default Navigation
