import Reactotron, { overlay } from 'reactotron-react-native'
import { reactotronRedux } from 'reactotron-redux'
import sagaPlugin from 'reactotron-redux-saga'

const reactotron = Reactotron
	.configure({ name: 'Exchange App' })
	.use(overlay())
	.use(sagaPlugin())
	.use(reactotronRedux())
	.connect()

console.logger = Reactotron.log
console.important = Reactotron.logImportant
console.display = Reactotron.display
console.tron = Reactotron

export default reactotron